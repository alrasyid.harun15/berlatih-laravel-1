<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::get('/', function () {
#    return view('welcome');
#});

use App\Http\Controllers\Home;
use App\Http\Controllers\Welcome;

Route::get('/', [Home::class,'show']);

Route::get('/Home', [Home::class,'show']);

Route::post('/Welcome', [Welcome::class,'confirm']);

Route::get('/Register', function () {return view('form');   
});