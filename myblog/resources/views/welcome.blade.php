<html>
    <head>
        <title>Selamat Datang di Sanber Book</title>
    </head>

    <body>
        <h1>SELAMAT DATANG!</h1>
        <h2>Terima Kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
        <br>
        <p>Data yang anda input adalah sebagai berikut!</p>
        <table border="1">
        	<tr>
        		<td>First Name</td><td>:</td><td>{{$first_name}}</td>
        	</tr>
        	<tr>
        		<td>last Name</td><td>:</td><td>{{$last_name}}</td>
        	</tr>
        	<tr>
        		<td>Gender</td><td>:</td><td>{{$gender}}</td>
        	</tr>
        	<tr>
        		<td>Nationality</td><td>:</td><td>{{$nationality}}</td>
        	</tr>
        	<tr>
        		<td>Language Spoken</td><td>:</td><td>{{$lang}}</td>
        	</tr>
        	<tr>
        		<td>Bio</td><td>:</td><td>{{$bio}}</td>
        	</tr>
        </table>
    </body>
</html>