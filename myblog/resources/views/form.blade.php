<html>
    <head>
        <title>Buat Account Baru!</title>
    </head>
    <body>

        <h2>Sign Up Form</h2>

        <form name="form_sign_up" action="Welcome" method="POST">

        @csrf

            <p>
                <label for="field_first_name_id">First Name :</label> 
                <br>
                <input type="text" name="field_first_name" id="field_first_name_id" value="">
            </p>

            <p>
                <label for="field_last_name_id">Last Name :</label>  
                <br>
                <input type="text" name="field_last_name" id="field_last_name_id" value="">
            </p>

            <p>
                <label>Gender :</label>  
                <br>
                <input type="radio" name="field_gender" value="male" checked> Male
                <br>
                <input type="radio" name="field_gender" value="female"> Female
            </p>

            <p>
                <label for="field_nationality_id">Nationality :</label>
                <br>
                <select name="field_nationality" id="field_nationality_id">
                    <option value="Indonesia">Indonesia</option>
                    <option value="Inggris">Inggris</option>
                    <option value="India">India</option>
                    <option value="00" selected>Pilih salah satu</option>
                </select>
                
                
            </p>

            <p>
                <label>Language Spoken :</label> 
                <br>
                <input type="checkbox" name="field_lang_spoken" value="Bahasa" checked> Bahasa Indonesia
                <br>
                <input type="checkbox" name="field_lang_spoken" value="English"> English
                <br>
                <input type="checkbox" name="field_lang_spoken" value="Other"> Other
            </p>

            <p>
                <label for="field_bio_id">Bio :</label>
                <br>
                <textarea name="field_bio" id="field_bio_id" cols="30" rows="10"></textarea>
            </p>

            <p>
                <input type="submit" value="Sign-up" >
            </p>

        </form>

    </body>
</html>