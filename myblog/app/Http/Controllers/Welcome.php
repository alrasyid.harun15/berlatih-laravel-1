<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Welcome extends Controller
{
    public function confirm(Request $request){

    	$first_name 	= $request->input('field_first_name');
		$last_name 		= $request->input('field_last_name');
		$gender 		= $request->input('field_gender');
		$nationality 	= $request->input('field_nationality');
		$lang_spoken 	= $request->input('field_lang_spoken');
		$bio 			= $request->input('field_bio');

    return view('welcome',[
    	'first_name'	=>$first_name,
    	'last_name'		=>$last_name, 
    	'gender'		=>$gender, 
    	'nationality'	=>$nationality, 
    	'lang'			=>$lang_spoken, 
    	'bio'			=>$bio]);
    }
}
